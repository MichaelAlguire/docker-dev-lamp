# Docker Dev LAMP
A basic LAMP stack that works with the [Docker Dev Controller](https://gitlab.com/MichaelAlguire/docker-dev-controller).

## Database
I currently don't have a script to dump the database.  Instead, there is a command you can run but it has to be customized.

```bash
docker exec dockerdevlamp /usr/bin/mysqldump myapp -u root -prootpw | Set-Content server/data/mysql/database.sql
```

As long as all the variables in the command are correct, this will work.  When you start your app next time, the database dump will be imported.

## Acknowledgements
[SprintCube / docker-compose-lamp](https://github.com/sprintcube/docker-compose-lamp) -  Used as a starting point