<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Docker Dev LAMP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<section class="py-5" style="background-color: lightblue;">
		<h1 class="text-center">Docker Dev LAMP</h1>
		<h2 class="text-center">Your local development environment</h2>
	</section>

	<section class="py-5">
		<div class="row">
			<div class="col-12 col-md-6">
				<h3>Environment</h3>
				<hr>
				<div>
					<ul>
						<li><?php echo apache_get_version(); ?></li>
						<li>PHP <?php echo phpversion(); ?></li>
						<li>
							<?php
							$link = mysqli_connect("mysql", "root", "rootpw", null);

							/* check connection */
							if (mysqli_connect_errno()) {
								printf("MySQL connecttion failed: %s", mysqli_connect_error());
							} else {
								/* print server version */
								printf("MySQL Server %s", mysqli_get_server_info($link));
							}
							/* close connection */
							mysqli_close($link);
							?>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<h3>Quick Links</h3>
				<hr>
				<div>
					<ul>
						<li><a href="/phpinfo.php">phpinfo()</a></li>
						<li><a href="http://localhost:8080">phpMyAdmin</a></li>
						<li><a href="/test_db.php">Test DB Connection</a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
</div>

</body>
</html>